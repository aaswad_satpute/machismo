//
//  CardGameAppDelegate.h
//  Machismo
//
//  Created by Aaswad Satpute on 19/12/13.
//  Copyright (c) 2013 AcjS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
