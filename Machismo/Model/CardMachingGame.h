//
//  CardMachingGame.h
//  Machismo
//
//  Created by Aaswad Satpute on 21/12/13.
//  Copyright (c) 2013 AcjS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMachingGame : NSObject
-(instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck;
-(void)chooseCardAtIndex:(NSUInteger)index;
-(Card *)cardAtIndex:(NSUInteger)index;
@property (nonatomic,readonly)NSInteger score;
@end
