//
//  Deck.h
//  Machismo
//
//  Created by Aaswad Satpute on 20/12/13.
//  Copyright (c) 2013 AcjS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject
-(void)addCard:(Card *)card atTop:(BOOL)atTop;
-(void)addCard:(Card *)card;
-(Card *)drawRandomCard;
@end
