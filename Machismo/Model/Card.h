//
//  Card.h
//  Machismo
//
//  Created by Aaswad Satpute on 19/12/13.
//  Copyright (c) 2013 AcjS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property (strong, nonatomic) NSString *contents;
@property (nonatomic, getter = isChosen) BOOL chosen;
@property (nonatomic, getter = isMatched) BOOL matched;
-(int)match:(NSArray *)otherCards;
@end
