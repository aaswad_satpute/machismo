//
//  PlayingCard.h
//  Machismo
//
//  Created by Aaswad Satpute on 20/12/13.
//  Copyright (c) 2013 AcjS. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card
@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSInteger rank;
+(NSArray *)validSuits;
+(NSUInteger)maxRank;
@end
